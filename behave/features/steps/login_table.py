from behave import given, when, then, step
import time

@given("go to the homepage of a agency with examples of table")
def step_impl_carga(context):
    context.driver.get(context.base_url)
       
@when('with table examples enter valid "{email}" and valid "{password}"')
def step_impl(context, email, password):
        txt_user = context.driver.find_element_by_id("tct-login-email")
        txt_user.send_keys(email)

        txt_pass = context.driver.find_element_by_id("tct-login-password")
        txt_pass.send_keys(password)

        login_btn = context.driver.find_element_by_id("tct-login-submit")
        login_btn.click()
    
@then("the user log in using examples of table")
def step_impl(context):
    time.sleep(3)
    txt_sesion = context.driver.find_element_by_id("main-dashboard")
    if txt_sesion:
        pass
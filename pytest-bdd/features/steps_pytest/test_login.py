import pytest
from pytest_bdd import given, when, then, scenarios, parsers
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

#constants
travelcontact = 'http://alanna.lvh.me:8000/login'

#scenarios
scenarios('../login.feature')

#fixture
@pytest.fixture
def browser():
    b = webdriver.Firefox()
    b.implicitly_wait(10)
    yield b
    b.quit()

#Given Steps
@given('go to the homepage of a agency')
def load_page(browser):
    browser.get(travelcontact)

#When Steps
@when(parsers.parse('submit "{email}" and "{password}"'))
def enter_credentials(browser, email, password):
    enter_email = browser.find_element_by_id('tct-login-email')
    enter_email.send_keys(email)

    enter_password = browser.find_element_by_id('tct-login-password')
    enter_password.send_keys(password + Keys.RETURN)

    #enter_btn = browser.find_element_by_id('tct-login-submit')

#Then Steps      
@then('the user logs in')
def visit_dashboard(browser):
    time.sleep(2)
    visit_dashboard = browser.find_element_by_id("main-dashboard")
    if visit_dashboard:
        pass

          
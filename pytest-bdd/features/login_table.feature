Feature: test table pytest

    The user initiates session in the application

Scenario Outline: correct domain and correct credentials
        Given go to the homepage of a agency with examples of table
        When with table examples enter valid <email> and valid <password>
        Then the user log in using examples of table

        Examples:
            | email                     | password   |
            | pablo.gerbasi@cleteci.com | teamtc2018 |
            | carlos@cleteci.com        | teamtc2018 |